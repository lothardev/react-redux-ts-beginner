import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getProducts, getTotalProducts } from './store/products.selectors';
import {
  addProductHttp,
  deleteProductHttp,
  getProductsHttp,
  toggleProductHttp,
} from './store/products.actions';
import { Product } from './model/product';


// TODOS Component
export const CatalogPage: React.FC<any> = () => {
  const products = useSelector(getProducts);
  const total = useSelector(getTotalProducts);
  const dispatch = useDispatch();

  useEffect(() => {
    setTimeout(() => {
      dispatch(getProductsHttp());
    }, 1000)
    // dispatch(thunkSendMessage());
  }, [dispatch]);

  const addTodoHandler = () => {
    dispatch(addProductHttp({
      title: Date.now().toString(),
      price: 10,
    }))
  };

  const deleteHandler = (event: React.MouseEvent<HTMLElement>, id: number) => {
    event.stopPropagation();
    dispatch(deleteProductHttp(id))
  };

  const toggleHandler = (todo: Product) => {
    dispatch(toggleProductHttp(todo));
  };

  return <div>
    <div>PRODUCT: {products.length}</div>

    <hr/>
    <button onClick={addTodoHandler}>ADD PRODUCT</button>
    {
      products.map((product) => {
        return (
          <li
            className="list-group-item"
            style={{ opacity: product.visibility ? 1 : 0.5}}
            key={product.id}
          >
            <span onClick={() => toggleHandler(product)}>
              {
                product.visibility ?
                  <i className="fa fa-eye" /> :
                  <i className="fa fa-eye-slash" />
              }
            </span>
            <span className="ml-2">{ product.title }</span>

            <div className="pull-right">
              { product.price }
              <i
                className="fa fa-trash "
                onClick={(e) => deleteHandler(e, product.id)}
              />
            </div>

          </li>
        )
      })
    }
    <div className="text-center">
    <div className="badge badge-info">Total: € {total}</div>
    </div>
  </div>
};

